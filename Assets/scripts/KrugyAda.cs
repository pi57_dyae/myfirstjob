﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KrugyAda : MonoBehaviour {
    public GameObject center;
    private int[][] krug = new int[6][];
    private int[] kek1 = new int[6] { 0, 0, 0, 0, 0, 0 };
    void Start() {
        for(int k = 0; k < 6; k++)
        {
            krug[k] = new int[] { 0, 0, 0, 0, 0, 0 };
        }
        fun();
    }
    void Update()
    {
        
    }
    public void fun()
    {
        for( int x = 0; x < 6; x++)
        {
            kek1[x] = 0;
        }
        int ran = Random.Range(0 ,6);//zarandomulu
        kek1[ran] = 1;
        
        for (int b = 0; b < 6; b++)
        {
            if (kek1[b] == 1)
            {
                center.transform.GetChild(b).gameObject.SetActive(true);
                //Debug.Log(center.transform.GetChild(b).gameObject.name);
            }
            else center.transform.GetChild(b).gameObject.SetActive(false);
        }
    }
    public void click(GameObject circle)
    {
        bool c = true;
        bool t = true;
        int n = int.Parse(circle.name[6].ToString());//distalu imja
        for(int i = 0; i < 6; i++)
        {                     
                if (kek1[i] == 1 && krug[n][i] == 1)
                {
                    c = false;
                }           
        }
        if(c)
        {
            for(int a = 0; a < 6; a++)
            {               
                if (kek1[a] == 1)
                    krug[n][a] = 1;
                if(krug[n][a]==0)
                {
                    t = false;
                }
            }//perezapusuemo masiv jakszo e kusoczok to ne stavut' jogo        
            kusoczku(circle);
            fun();
            if (t)
            {
                for (int i = 0; i < 6; i++)
                {
                    krug[n][i] = 0;
                }
            }
            kusoczku(circle);
        }        
    }
    public void kusoczku(GameObject circle)
    {
        int n = int.Parse(circle.name[6].ToString());
        for(int b = 0; b < 6; b++)
        {
            if(krug[n][b] == 1)
            {
                //Debug.Log(circle.transform.GetChild(b).gameObject.name);
                circle.transform.GetChild(b).gameObject.SetActive(true);
            }else circle.transform.GetChild(b).gameObject.SetActive(false);
        }
    }
}
